import React, { Component } from 'react'

export default class SecHead extends Component {
    render() {
        const { heading, subHeading } = this.props
        return (
            <div className="sec-head sec-head-common-2">
                <div className="heading">
                    {heading}
                </div>
                <div className="sub-heading">
                    {subHeading}
                </div>
            </div>
        )
    }
}
