class VariableEx {
    isEmpty = (object?: any): boolean => {
        if ((object == undefined)
            || (object == {})
            || (object == [])
            || (object == null)
            || (object == "")) {
            return true
        }

        return (!object)
    }

    isEmail = (str: string): boolean => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(str)) {
            return (true)
        }

        return (false)
    }
}

const variableEx = new VariableEx()

export default variableEx